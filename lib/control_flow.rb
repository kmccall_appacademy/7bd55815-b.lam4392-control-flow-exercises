# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  new_str = ''
  str.each_char { |ch| new_str << ch if ch.downcase != ch }
  new_str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  mid_index = str.length / 2
  if str.length.odd?
    return str[mid_index]
  else
    return str[mid_index - 1] + str[mid_index]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  num_vowels = 0
  str.each_char { |ch| num_vowels += 1 if VOWELS.include?(ch)}
  num_vowels
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  factorial = 1
  (1..num).each { |num| factorial *= num}
  factorial
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  my_join = ''
  arr.each_index do |index|
    if index == arr.length - 1
      my_join << arr[index]
    else
      my_join << arr[index] << separator
    end
  end
  my_join
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  weirdcase_str = ''
  str.each_char.with_index do |ch, index|
      if index.even?
        weirdcase_str << ch.downcase
      else
        weirdcase_str << ch.upcase
      end
  end
  weirdcase_str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str_array = str.split
  str_array.each.with_index do |word, index|
    str_array[index] = word.reverse if word.length > 4
  end
  str_array.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  fizzbuzz_array = []
  (1..n).each do |num|
    if (num % 3).zero? && (num % 5).zero?
      fizzbuzz_array << 'fizzbuzz'
    elsif (num % 3).zero?
      fizzbuzz_array << 'fizz'
    elsif (num % 5).zero?
      fizzbuzz_array << 'buzz'
    else
      fizzbuzz_array << num
    end
  end
  fizzbuzz_array
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num <= 1
  (2...num).each do |divisor|
    return false if (num % divisor).zero?
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors = []
  (1..num).each { |divisor| factors << divisor if (num % divisor).zero? }
  factors
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors = factors(num)
  prime_factors = []
  factors.each { |factor| prime_factors << factor if prime?(factor) }
  prime_factors
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd_arr = []
  even_arr = []
  arr.each do |num|
    if num.odd?
      odd_arr << num
    else
      even_arr << num
    end
  end
  return odd_arr[0] if odd_arr.length == 1
  even_arr[0]
end
